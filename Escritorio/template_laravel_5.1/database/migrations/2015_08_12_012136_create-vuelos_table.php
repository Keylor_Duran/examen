<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVuelosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vuelos', function (Blueprint $table) {
              $table->increments('id');
             $table->integer('piloto_id')->unsigned();
              $table->integer('avion_id')->unsigned();

            $table->foreign('piloto_id')
      ->references('id')->on('pilotos')
      ->onDelete('cascade');

      $table->foreign('avion_id')
      ->references('id')->on('aviones')
      ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('vuelos');
    }
}
