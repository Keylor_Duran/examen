<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Avion extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'Aviones';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'placa',
        'color'

        ];
    /**
     * Mutators
     *
     * @param $date
     */
    public function setPublishedAtAttribute($date)
    {
        //$this->attributes['published_at'] = Carbon::createFromFormat('Y-m-d', $date);
        $this->attributes['published_at'] = Carbon::parse($date);
    }
     //esto sirve para poner bien la fecha en la aplicacion 

    /**
     * Scope example
     *
     * @param $query
     */
    public function scopePublished($query)
    {
        $query->where('published_at', '<=', Carbon::now())->get();
    }
     //

    /**
     * Scope example
     *
     * @param $query
     */
    public function scopeUnpublished($query)
    {
        $query->where('published_at', '>', Carbon::now())->get();
    }

}