<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Piloto;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class PilotosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
       // $pilotos = Piloto::latest('published_at')
         //->published()->get(); //select a una base de datos invoca al scope del modelo
               //retornando una vista
        return view('pilotos.index');
    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('pilotos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
          Piloto::create($request->all());
        return redirect('pilotos');
    }
        public function store1(Resquest $request)
    {
        $rules=[
        'nombre'=>'required|min:3',
        ];
        $this->validate($request,$rules);

        Piloto::create($request->all());
        return redirect('pilotos');
    }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
           $pilotos= Piloto::find($id);
        if(is_null($article)){abort(404);}
        return view('pilotos.show',compact('pilotos'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
         $Piloto=piloto::findOrFail($id);

        return view('piloto.edit',compact('piloto'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id,CreatePilotoRequest $request)
    {
       $piloto=piloto::findOrFail($id);
       $piloto->update($request->all());
       return redirect('piloto');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $piloto=piloto::findOrFail($id);
        $piloto->delete();

        return redirect('pilotos');
    }
}
}
