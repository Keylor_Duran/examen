<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Piloto;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class PilotosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
       // $pilotos = Piloto::latest('published_at')
         //->published()->get(); //select a una base de datos invoca al scope del modelo
               //retornando una vista
        return view('aviones.index');
    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('aviones.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
          Avion::create($request->all());
        return redirect('aviones');
    }
        public function store1(Resquest $request)
    {
        $rules=[
        'nombre'=>'required|min:3',
        ];
        $this->validate($request,$rules);

        Piloto::create($request->all());
        return redirect('aviones');
    }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
           $aviones= Avion::find($id);
        if(is_null($avion)){abort(404);}
        return view('avion.show',compact('Avion'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
         $Avion=avion::findOrFail($id);

        return view('avion.edit',compact('avion'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id,CreatePilotoRequest $request)
    {
       $avion=avion::findOrFail($id);
       $avion->update($request->all());
       return redirect('avion');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $avion=piloto::findOrFail($id);
        $avion->delete();

        return redirect('aviones');
    }
}