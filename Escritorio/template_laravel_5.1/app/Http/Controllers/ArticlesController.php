<?php

namespace App\Http\Controllers;
use App\Http\Requests\CreateArticleRequest;
use Illuminate\Http\Request;
use App\Article;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class ArticlesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //recupera todo los articulos
         $articles = Article::latest('published_at')
         ->published()->get(); //select a una base de datos invoca al scope del modelo
               //retornando una vista
        return view('articles.index',compact('articles'));
    
    }
      
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('articles.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(CreateArticleRequest $request)
    {
        Article::create($request->all());
        return redirect('articles');
    }
        public function store1(Resquest $request)
    {
        $rules=[
        'title'=>'required|min:3',
        'body' =>'required',
        'published_at'=>'required|date'
        ];
        $this->validate($request,$rules);

        Article::create($request->all());
        return redirect('articles');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $article= Article::find($id);
        if(is_null($article)){abort(404);}
        return view('articles.show',compact('article'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
         $article=article::findOrFail($id);

        return view('articles.edit',compact('article'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id,CreateArticleRequest $request)
    {
       $article=article::findOrFail($id);
       $article->update($request->all());
       return redirect('article');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $article=article::findOrFail($id);
        $article->delete();

        return redirect('articles');
    }
}
