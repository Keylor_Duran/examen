@extends('app')

@section('content')
    <h1>Name a new Piloto</h1>
    <hr>

    {!! Form::open(['url' => 'pilotos']) !!}
        @include('pilotos.form',['submitButtonText'=>'Agregar Piloto'])
    {!! Form::close() !!}

    @include('errors.list')

@stop