@extends('app')

@section('content')
    <h1>Pilotos</h1>

    @foreach($pilotos as $piloto)
        <article>

            <a href="/pilotos/{{ $piloto->id  }}"><h2>{{ $piloto->title }}</h2></a>

            {{--
                <a href="{{ action('PilotosController@show', [$piloto->id]) }}"><h2>{{ $article->title }}</h2></a>
                <a href="{{ url('/pilotos', $piloto->id) }}"><h2>{{ $article->title }}</h2></a>
            --}}

            <div class="body">
                {{ $piloto->body }}
            </div>
        </article>
    @endforeach

    <a class="btn btn-primary btn-lg" href="{{ url('pilotos/create') }}"
       role="button">Nombre del Piloto</a>
@stop