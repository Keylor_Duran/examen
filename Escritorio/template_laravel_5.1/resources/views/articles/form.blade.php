<!-- Title Form Input -->
<div class="form-group">
    {!! Form::label('nombre','Nombre: ')  !!}
    {!! Form::text('nombre', null , ['class'=>'form-control'])  !!}
</div>

<!-- Submit -->
<div class="form-group">
    {!! Form::submit($submitButtonText, ['class'=>'btn btn-primary form-control'])  !!}
</div>