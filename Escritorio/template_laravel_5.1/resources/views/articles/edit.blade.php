@extends('app')

@section('content')
    <h1>Editando: {!! $article->title  !!}</h1>
    <hr>

    {!! Form::model($article, ['method'=>'PATCH', 'action' => ['ArticlesController@update', $article->id]]) !!}
        @include('articles.form',['submitButtonText'=>'Actualizar artículo'])
    {!! Form::close() !!}

    @include('errors.list')

@stop